﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IntegratedCore.Dtos;
using IntegratedCore.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace IntegratedApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticleController : ControllerBase
    {
        private readonly IArticlesServices _articlesServices;

        public ArticleController(IArticlesServices articlesServices)
        {
            _articlesServices = articlesServices;
        }

        [HttpPost("submit")]
        public async Task<IActionResult> saveArticle(ArticleDto articleDto)
        {
            var response = await _articlesServices.Submit(articleDto);
            return Ok(response);
        }

        [HttpPut("edit/{id}")]
        public async Task<IActionResult> editArticle(int id, ArticleDto articleDto)
        {
            var response = await _articlesServices.Edit(id, articleDto);
            return Ok(response);
        }

        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> deleteArticle(int id, ArticleDto articleDto)
        {
            var response = await _articlesServices.Delete(id, articleDto);
            return Ok(response);
        }

        [HttpGet("all")]
        public async Task<IActionResult> getArticles()
        {
            var response = await _articlesServices.GetArticles();
            return Ok(response);
        }
    }
}
