﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IntegratedCore.Dtos;
using IntegratedCore.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace IntegratedApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorController : ControllerBase
    {
        private readonly IAuthorsService _authorService;

        public AuthorController(IAuthorsService authorsService)
        {
            _authorService = authorsService;
        }

        [HttpPost("authenticate")]
        public async Task<IActionResult> authenticate(AuthDto authDto)
        {
            var response = await _authorService.Aunthenticate(authDto.Username, authDto.Password);
            return Ok(response);
        }

        [HttpPost("signUp")]
        public async Task<IActionResult> signUp(SignUpDto signUpDto)
        {
            var response = await _authorService.SignUp(signUpDto);
            return Ok(response);
        }

        [HttpGet]
        public async Task<IActionResult> getAuthors()
        {
            var response = await _authorService.GetAuthors();
            return Ok(response);
        }

    }
}
