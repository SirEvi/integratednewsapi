﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegratedCore.Dtos
{
    public class AuthDto
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
