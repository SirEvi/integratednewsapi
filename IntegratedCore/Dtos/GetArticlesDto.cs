﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegratedCore.Dtos
{
    public class GetArticlesDto
    {
        public int id { get; set; }
        public string title { get; set; }
        public string headline { get; set; }
        public string content { get; set; }
        public string email { get; set; }
        public DateTime addedAt { get; set; }
    }
}
