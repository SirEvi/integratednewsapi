﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegratedCore.Dtos
{
    public class SignUpDto
    {
        public string name { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string bio { get; set; }

    }
}
