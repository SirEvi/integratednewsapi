﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegratedCore.Dtos
{
    public class ArticleDto
    {
        public string title { get; set; }
        public string headline { get; set; }
        public string content { get; set; }
        public string email { get; set; }
    }
}
