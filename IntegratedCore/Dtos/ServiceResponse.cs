﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegratedCore.Dtos
{
    public class ServiceResponse
    {
        public string statusMessage { get; set; }
        public int responseCode { get; set; }
        public List<GetAuthorsDto> data { get; set; }
    }   
}
