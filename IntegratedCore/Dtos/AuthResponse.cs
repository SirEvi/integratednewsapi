﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegratedCore.Dtos
{
    public class AuthResponse
    {
        public string statusMessage { get; set; }
        public int responseCode { get; set; }
        public object data { get; set; }
        public string accessToken { get; set; }
    }
}
