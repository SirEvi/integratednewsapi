﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegratedCore.Dtos
{
    public class GetAuthorsDto
    {
        public int id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string bio { get; set; }
    }
}
