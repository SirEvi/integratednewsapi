﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntegratedCore.Dtos
{
    public class ArticlesResponse
    {
        public string statusMessage { get; set; }
        public int responseCode { get; set; }
        public List<GetArticlesDto> data { get; set; }
    }
}
