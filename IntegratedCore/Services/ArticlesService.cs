﻿using IntegratedCore.Dtos;
using IntegratedCore.Interface;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace IntegratedCore.Services
{
    public class ArticlesService : IArticlesServices
    {
        private readonly IConfiguration _config;
        private readonly IHttpClientFactory _clientFactory;

        public ArticlesService(IConfiguration config, IHttpClientFactory clientFactory)
        {
            _config = config;
            _clientFactory = clientFactory;
        }
        public async Task<ServiceResponse> Submit(ArticleDto articleDto)
        {
            try
            {
                if (articleDto != null)
                {
                    var jsonObject = JsonConvert.SerializeObject(articleDto);

                    var accessToken = _config.GetValue<string>("LoginCredentials:jwt");
                    var resourceuri = _config.GetValue<string>("Hassan:baseUrl");
                    var save = _config.GetValue<string>("Hassan:save");

                    var client = _clientFactory.CreateClient("API Client");
                    var url = resourceuri + save;
                    var request = new HttpRequestMessage(HttpMethod.Post, url);

                    request.Content = new StringContent(jsonObject);
                    request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    request.Headers.Add("Authorization", "Bearer " + accessToken);


                    var response = await client.SendAsync(request);
                    var content = await response.Content.ReadAsStringAsync();

                    if (response != null)
                    {
                        return JsonConvert.DeserializeObject<ServiceResponse>(content);
                    }
                    else
                    {
                        throw new Exception("Error occurred");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error : {0}", ex.Message);
            }

            return null;
        }

        public async Task<ServiceResponse> Edit(int Id, ArticleDto articleDto)
        {
            try
            {
                if (articleDto != null)
                {
                    var jsonObject = JsonConvert.SerializeObject(articleDto);

                    var accessToken = _config.GetValue<string>("LoginCredentials:jwt");
                    var resourceuri = _config.GetValue<string>("Hassan:baseUrl");
                    var edit = _config.GetValue<string>("Hassan:edit");
                    int id = Id;

                    var client = _clientFactory.CreateClient("API Client");
                    var url = resourceuri + edit + "/" + id;
                    var request = new HttpRequestMessage(HttpMethod.Put, url);

                    request.Content = new StringContent(jsonObject);
                    request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    request.Headers.Add("Authorization", "Bearer " + accessToken);


                    var response = await client.SendAsync(request);
                    var content = await response.Content.ReadAsStringAsync();

                    if (response != null)
                    {
                        return JsonConvert.DeserializeObject<ServiceResponse>(content);
                    }
                    else
                    {
                        throw new Exception("Error occurred");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error : {0}", ex.Message);
            }

            return null;
        }

        public async Task<ServiceResponse> Delete(int Id, ArticleDto articleDto)
        {
            try
            {
                if (articleDto != null)
                {
                    var jsonObject = JsonConvert.SerializeObject(articleDto);

                    var accessToken = _config.GetValue<string>("LoginCredentials:jwt");
                    var resourceuri = _config.GetValue<string>("Hassan:baseUrl");
                    var delete = _config.GetValue<string>("Hassan:delete");

                    var client = _clientFactory.CreateClient("API Client");
                    var url = resourceuri + delete + "/" + Id;
                    var request = new HttpRequestMessage(HttpMethod.Delete, url);

                    request.Content = new StringContent(jsonObject);
                    request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    request.Headers.Add("Authorization", "Bearer " + accessToken);


                    var response = await client.SendAsync(request);
                    var content = await response.Content.ReadAsStringAsync();

                    if (response != null)
                    {
                        return JsonConvert.DeserializeObject<ServiceResponse>(content);
                    }
                    else
                    {
                        throw new Exception("Error occurred");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error : {0}", ex.Message);
            }

            return null;
        }

        public async Task<ArticlesResponse> GetArticles()
        {
            try
            {
                var resourceuri = _config.GetValue<string>("Hassan:baseUrl");
                var getAll = _config.GetValue<string>("Hassan:getAll");

                var client = _clientFactory.CreateClient("API Client");
                var url = resourceuri + getAll;
                var request = new HttpRequestMessage(HttpMethod.Get, url);

                var response = await client.SendAsync(request);
                var content = await response.Content.ReadAsStringAsync();

                if (content != null)
                {
                    return JsonConvert.DeserializeObject<ArticlesResponse>(content);
                }
                else
                {
                    throw new Exception("Error occurred");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error : {0}", ex.Message);
            }

            return null;
        }
    }
}
