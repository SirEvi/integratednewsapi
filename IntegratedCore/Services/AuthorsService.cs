﻿using IntegratedCore.Dtos;
using IntegratedCore.Interface;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace IntegratedCore.Services
{
    public class AuthorsService : IAuthorsService
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly IConfiguration _config;

        public AuthorsService(IHttpClientFactory clientFactory, IConfiguration config)
        {
            _clientFactory = clientFactory;
            _config = config;
        }
        public async Task<AuthResponse> Aunthenticate(string username, string password)
        {
            try
            {
                var body = new
                {
                    //username = _config.GetValue<string>("LoginCredentials:username"),
                    //password = _config.GetValue<string>("LoginCredentials:password")
                    username = username,
                    password = password
                };

                if (body != null)
                {
                    var jsonObject = JsonConvert.SerializeObject(body);

                    var resourceuri = _config.GetValue<string>("Hassan:baseUrl");
                    var authenticate = _config.GetValue<string>("Hassan:authenticate");

                    var client = _clientFactory.CreateClient("API Client");
                    var url = resourceuri + authenticate;
                    var request = new HttpRequestMessage(HttpMethod.Post, url);
                    
                    request.Content = new StringContent(jsonObject);
                    request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    

                    var response = await client.SendAsync(request);
                    var content = await response.Content.ReadAsStringAsync();
                    AuthResponse jsondeserialObj;

                    if (response != null)
                    {
                        jsondeserialObj = JsonConvert.DeserializeObject<AuthResponse>(content);

                        var authheader = jsondeserialObj.accessToken.ToString();
                        if (!string.IsNullOrWhiteSpace(authheader))
                            AddOrUpdateAppSetting("UserCredentials:jwt", authheader);

                        return jsondeserialObj;
                    }
                    else
                    {
                        throw new Exception("Error occurred");
                    }                    
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error : {0}", ex.Message);
            }

            return null;
        }

        public async Task<ServiceResponse> GetAuthors()
        {
            try
            {
                var resourceuri = _config.GetValue<string>("Hassan:baseUrl");
                var authors = _config.GetValue<string>("Hassan:authors");

                var client = _clientFactory.CreateClient("API Client");
                var url = resourceuri + authors;
                var request = new HttpRequestMessage(HttpMethod.Get, url);

                var response = await client.SendAsync(request);
                var content = await response.Content.ReadAsStringAsync();

                if (content != null)
                {
                    return JsonConvert.DeserializeObject<ServiceResponse>(content);
                }
                else
                {
                    throw new Exception("Error occurred");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error : {0}", ex.Message);
            }

            return null;
        }

        public async Task<ServiceResponse> SignUp(SignUpDto signUpDto)
        {
            try
            {
                if (signUpDto != null)
                {
                    var jsonObject = JsonConvert.SerializeObject(signUpDto);

                    var resourceuri = _config.GetValue<string>("Hassan:baseUrl");
                    var signUp = _config.GetValue<string>("Hassan:signUp");

                    var client = _clientFactory.CreateClient("API Client");
                    var url = resourceuri + signUp;
                    var request = new HttpRequestMessage(HttpMethod.Post, url);

                    request.Content = new StringContent(jsonObject);
                    request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");


                    var response = await client.SendAsync(request);
                    var content = await response.Content.ReadAsStringAsync();

                    if (response != null)
                    {
                        return JsonConvert.DeserializeObject<ServiceResponse>(content);
                    }
                    else
                    {
                        throw new Exception("Error occurred");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error : {0}", ex.Message);
            }

            return null;
        }

         public static void AddOrUpdateAppSetting<T>(string key, T value)
        {
            try
            {
                var filePath = Path.Combine(AppContext.BaseDirectory, "appSettings.json");
                string json = File.ReadAllText(filePath);
                dynamic jsonObj = JsonConvert.DeserializeObject(json);

                var sectionPath = key.Split(":")[0];
                if (!string.IsNullOrEmpty(sectionPath))
                {
                    var keyPath = key.Split(":")[1];
                    jsonObj[sectionPath][keyPath] = value;
                }
                else
                {
                    jsonObj[sectionPath] = value; // if no sectionpath just set the value
                }
                string output = JsonConvert.SerializeObject(jsonObj, Formatting.Indented);
                File.WriteAllText(filePath, output);

            }
            catch (Exception ex)
            {
                Console.WriteLine(@"Error writing app settings: {0}", ex.Message);
            }
        }
    }
}
