﻿using IntegratedCore.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IntegratedCore.Interface
{
    public interface IAuthorsService
    {
        Task<AuthResponse> Aunthenticate(string username, string password);
        Task<ServiceResponse> SignUp(SignUpDto signUpDto);
        Task<ServiceResponse> GetAuthors();
    }
}
