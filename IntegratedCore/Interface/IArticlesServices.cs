﻿using IntegratedCore.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IntegratedCore.Interface
{
    public interface IArticlesServices
    {
        Task<ServiceResponse> Submit(ArticleDto articleDto);
        Task<ServiceResponse> Edit(int id, ArticleDto articleDto);
        Task<ServiceResponse> Delete(int id, ArticleDto articleDto);
        Task<ArticlesResponse> GetArticles();

    }
}
